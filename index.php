<?php

//Class usada pra fazer o login
require('assets/class/Login.php');
session_start();

$class = new Login();
//VERIFICA SE O IP ESTA BLOQUEADO
$class->bloqueioIp($_SERVER["REMOTE_ADDR"]);


if ($_POST)

{
  $retorno =$class->loginVerifica(($_POST));
}





?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="LUCAS GHILARDI">
    <meta name="keyword" content="TESTE WEBJUMP">

    <title>Login - Sistema GoJumper</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <!-- Custom styles for this template -->
    <link href="assets/css/style-login.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
    <script src='https://www.google.com/recaptcha/api.js'></script>



</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="/" method="POST">
        <h2 class="form-signin-heading">Go Jumper</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="User" name='user' autofocus>
            <input type="password" class="form-control" placeholder="Password" name='pass'>
            <div class="g-recaptcha" data-sitekey="6LfBzvcUAAAAAENiAuNNPI_hi0P33z22AcnQbgyQ"></div><br><br>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
            

        </div>

          <!-- Modal -->
          
          <!-- modal -->

      </form>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="admin/html/js/jquery.js"></script>
    <script src="admin/html/js/bootstrap.bundle.min.js"></script>


  </body>
</html>
