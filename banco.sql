-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 16, 2020 at 10:47 AM
-- Server version: 10.3.23-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webjump_sistema`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(4) NOT NULL,
  `name` varchar(30) NOT NULL,
  `cod` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `cod`) VALUES
(6, 'amarelo', 'amarelo'),
(8, 'azul', 'azul'),
(9, 'vermelho', 'vermelho');

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE `category_products` (
  `id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `produtcs_id` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `category_id`, `produtcs_id`) VALUES
(17, 5, 18),
(16, 1, 17),
(37, 9, 2),
(36, 8, 2),
(35, 6, 2),
(18, 6, 18),
(19, 7, 18),
(39, 9, 19),
(38, 6, 19),
(27, 6, 20),
(26, 5, 20),
(42, 8, 21),
(40, 6, 9),
(41, 8, 22);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(2) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `ativo` int(1) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `vendedor` int(1) NOT NULL,
  `nivel` int(1) NOT NULL DEFAULT 0,
  `telefone` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `usuario`, `email`, `data`, `ativo`, `senha`, `nome`, `vendedor`, `nivel`, `telefone`) VALUES
(1, 'lucas', 'lucasghilardi@movidoaweb.com.br', '2019-06-05', 1, '34707e8bb09ee1f7204386fa254f836a', 'Lucas Maturo Ghilardi', 0, 1, '(11)94955-2601');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(5) NOT NULL,
  `id_usuario` int(2) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `data_fim` datetime NOT NULL,
  `data_inicio` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0 nao / 1 logou',
  `erro` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `id_usuario`, `ip`, `data_fim`, `data_inicio`, `status`, `erro`) VALUES
(1, 1, '179.110.13.8', '2020-05-15 23:22:47', '2020-05-15 23:22:47', 1, ''),
(2, 1, '179.110.13.8', '2020-05-16 00:03:36', '2020-05-16 00:03:36', 1, ''),
(3, 1, '179.110.13.8', '2020-05-16 00:03:56', '2020-05-16 00:03:56', 1, ''),
(4, 1, '179.110.13.8', '2020-05-16 00:08:58', '2020-05-16 00:08:58', 1, ''),
(5, 1, '179.110.13.8', '2020-05-16 00:29:19', '2020-05-16 00:29:19', 1, ''),
(6, 1, '179.110.13.8', '2020-05-16 00:36:14', '2020-05-16 00:36:14', 1, ''),
(7, 1, '179.110.13.8', '2020-05-16 01:06:55', '2020-05-16 01:06:55', 1, ''),
(8, 1, '179.110.13.8', '2020-05-16 01:11:26', '2020-05-16 01:11:26', 1, ''),
(9, 1, '179.110.13.8', '2020-05-16 01:28:52', '2020-05-16 01:28:52', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `log_dados`
--

CREATE TABLE `log_dados` (
  `id` int(8) NOT NULL,
  `id_usuario` int(3) NOT NULL,
  `id_modificado` varchar(10) NOT NULL,
  `tabela` varchar(10) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `data_modifica` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_dados`
--

INSERT INTO `log_dados` (`id`, `id_usuario`, `id_modificado`, `tabela`, `tipo`, `data_modifica`) VALUES
(1, 2, '3', 'category', 'editar', '2020-05-15 21:25:08'),
(2, 1, '0', 'category', 'apagar', '2020-05-15 21:29:48'),
(3, 1, '0', 'category', 'apagar', '2020-05-15 21:30:48'),
(4, 1, 'verde', 'category', 'apagar', '2020-05-15 21:37:13'),
(5, 1, '3232', 'category', 'insert', '2020-05-15 21:39:43'),
(6, 1, 'roxo', 'category', 'update', '2020-05-15 21:39:57'),
(7, 1, 'roxo', 'category', 'update', '2020-05-15 21:41:15'),
(8, 1, 'roxo', 'category', 'update', '2020-05-15 21:48:55'),
(9, 1, 'novo', 'category', 'insert', '2020-05-15 21:49:01'),
(10, 1, '22', 'products', 'insert', '2020-05-15 21:53:47'),
(11, 1, 'roxo', 'category', 'apagar', '2020-05-15 22:29:03'),
(12, 1, 'amarelo', 'category', 'update', '2020-05-15 22:29:09'),
(13, 1, '2', 'products', 'update', '2020-05-15 22:29:48'),
(14, 1, '22', 'products', 'update', '2020-05-15 22:30:34'),
(15, 1, '19', 'products', 'update', '2020-05-15 22:55:46'),
(16, 1, '9', 'products', 'update', '2020-05-15 22:55:57'),
(17, 1, '22', 'products', 'update', '2020-05-15 22:56:15'),
(18, 1, '21', 'products', 'update', '2020-05-15 22:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(5) NOT NULL,
  `sku` varchar(10) NOT NULL,
  `name` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(5) NOT NULL,
  `description` varchar(255) NOT NULL,
  `arquivo` varchar(100) NOT NULL,
  `data_cadastro` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `quantity`, `description`, `arquivo`, `data_cadastro`) VALUES
(2, 'lucas', 'Camiseta', 20.00, 23, 'teste2\r\n\r\nteste', 'upload/Captura de Tela 2020-05-15 aÌ€s 20.30.26.png', '2020-05-15 15:29:33'),
(22, 'lucas2', 'Bermuta', 22.22, 11, 'teste', 'upload/Captura de Tela 2020-05-13 aÌ€s 16.10.56.png', '2020-05-15 21:53:47'),
(9, '423423', 'Sapato', 34234.23, 0, 'deasc', '', '2020-05-15 15:43:53'),
(13, 'novo', 'Calça', 33.33, 5, 'teste', '', '2020-05-15 17:34:19'),
(14, 'teste', 'Teclado', 33.33, 99, 'teste', '', '2020-05-15 17:34:58'),
(15, 'arvore', 'Celular', 33.33, 111, 'teste', '', '2020-05-15 17:37:30'),
(16, 'novo2', 'Teste', 44.44, 1231, 'teste', '', '2020-05-15 17:47:30'),
(18, 'nosokekek', 'Camiseta Verde', 21.11, 99, 'teste', '', '2020-05-15 18:23:09'),
(19, '23423', 'Tennis', 3.44, 22, 'teste', '', '2020-05-15 18:24:40'),
(20, 'teste1114', 'Cinto', 44.44, 44, 'teste', 'upload/Captura de Tela 2020-03-15 aÌ€s 23.38.18.png', '2020-05-15 18:25:53'),
(21, 'nosasa', 'Iphone', 22.22, 444, 'teste', 'upload/Captura de Tela 2020-05-04 aÌ€s 10.22.06.png', '2020-05-15 18:27:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod` (`cod`);

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_dados`
--
ALTER TABLE `log_dados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `log_dados`
--
ALTER TABLE `log_dados`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
