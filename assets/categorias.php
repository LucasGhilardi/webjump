<?php
require('class/Geral.php');


$class = new Geral();
$Login = new Login();

//VERIFICA SE O IP ESTA BLOQUEADO
$Login->bloqueioIp($_SERVER["REMOTE_ADDR"]);
$Login->verificaLogin();




//verifica se o usuario clicou em deletar e roda function
if ($_GET['acao']=='deletar')
{
  $class->apagarCategoria($_GET['cod']);
}



//Puxa dados da tabela category
$query = $class->listarTabela('category');



?>



<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <?php include('include/header.php'); ?>
  <!-- Header -->
  <?php include('include/superior.php'); ?>
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="AddCategoria" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
<?php

while ($dados = $query->fetch_object()) { ?>

      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$dados->name?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$dados->cod?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span><a href='AddCategoria?id=<?=$dados->cod?>' >Edit</a></span></div>
            <div class="action delete"><span><a href='#' onclick="deleta('<?=$dados->cod?>')"> Delete</span></div>
          </div>
        </td>
      </tr>

    <? } ?>
     
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php include('include/footer.php'); ?>
<script type="text/javascript">

  
  function deleta(cod)
  {

var r = confirm("Deseja Excluir ? ");
if (r == true) {
 window.location='categorias?cod='+cod+'&acao=deletar';
} 
  }
</script>
 <!-- Footer --></body>

</html>
