<?php
include('class/Geral.php');


$class = new Geral();
$Login = new Login();
//VERIFICA SE O IP ESTA BLOQUEADO
$Login->bloqueioIp($_SERVER["REMOTE_ADDR"]);
$Login->verificaLogin();





//Verifica se esta passando ID via GET, se tiver grava valor update no campo update hidden
if (isset($_GET['id']))
{
   $var='update';
   $query = $class->dadosProdutos($_GET['id']);
   $dados = $query->fetch_object();

}


//lista todas categorias cadastradas
$categorias = $class->listarTabela('category');


?>



<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <?php include('include/header.php'); ?>
  <!-- Header -->
  <?php include('include/superior.php'); ?>
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form id='formulario' method="POST" enctype="multipart/form-data">
      <input type="hidden" name="update" id='update' value="<?=$var?>">
      <input type="hidden" name="produtcs_id" value="<?=$dados->id?>">
      
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name='sku' class="input-text" value="<?=$dados->sku?>" <? if ($var=='update') echo 'readonly'; ?> /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name='name'class="input-text" value="<?=$dados->name?>"/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" value="<?=$dados->price?>" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="number" id="quantity" name="quantity" class="input-text" value="<?=$dados->quantity?>"/> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="category[]" class="input-text">
          <?php

           while ($dados2 = $categorias->fetch_object()) { 

            //verifica se a categoria foi selecionada
            $verifica_c= $class->verificaCategoria($dados->id,$dados2->id);
            if ($verifica_c=='1')
              $sel='selected';
            else
              $sel=''
            ?>
            <option value="<?=$dados2->id?>" <?=$sel?> ><?=$dados2->name?></option>


          <? } ?>





        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"><?=$dados->description?></textarea>
      </div>
      <div class="input-field">
        <label for="description" class="label">Image Product</label>
        <input type="file" class="input" name="arquivo" id='arquivo'  accept="image/png, image/jpeg">
        <?php if ($dados->arquivo!='')
        echo "<img src='".$dados->arquivo."' />"; ?>
      </div>
      <div class="actions-form">
        <a href="produtos" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php include('include/footer.php'); ?>
 <!-- Footer -->
</body>
   <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
   <script type="text/javascript" src="js/jquery.maskMoney.js"></script>
   
   <script type="text/javascript">
     $( document ).ready(function() {


         $('#price').maskMoney();



//Verifica se SKU já existe no banco de dados
      $('#sku').change(function() {
          var sku=$('#sku').val();
                      $.ajax({
                          url: 'verifica_sku.php',
                          dataType: 'html',
                          type: 'POST',
                          data: {sku :sku},
                         
                          success: function(data) {

                            if (data=='1')
                            {
                              alert('SKU JÁ CADASTRADO');
                              $('#sku').css({'border': '1px solid red'});          
                              $('#sku').focus();
                            }
                            else
                            {
                              $('#sku').css({'border': '1px solid green'});
                            }                

                          },
                          error: function(xhr,er) {
                             alert(xhr);
                             alert(er);
                          }
                      }); 
                  })
      $('#formulario').submit(function(event) {
          event.preventDefault();

          if ($('#sku').val()=='')
          {
            alert('Digite o SKU do produto');
            $('#sku').focus();
            return false
          }

          if ($('#name').val()=='')
          {
            alert('Digite o nome do produto');
            $('#name').focus();
            return false
          }
          if ($('#price').val()=='')
          {
            alert('Digite o Preço do produto');
            $('#price').focus();
            return false
          }
          if ($('#quantity').val()=='')
          {
            alert('Digite a quantidade do produto');
            $('#quantity').focus();
            return false
          }
          if ($('#description').val()=='')
          {
            alert('Digite a descrição do produto');
            $('#description').focus();
            return false
          }




                      var formData = new FormData($('form')[0]);


                      $.ajax({
                          url: 'addProduto_cadastro.php',
                          dataType: 'html',
                          type: 'POST',
                          data: formData,
                          contentType: false,
                          processData: false,
 
                         
                          success: function(data) {

                           

                            if (data=='errosku')
                            {
                              alert('SKU já cadastrado');
                              $('#sku').focus();
                            }
                            else
                            {
                              alert("Produto Salvo");
                              //Apaga os campos
                              $('#price').val('');
                              $('#name').val('');
                              $('#sku').val('');
                              $('#description').val('');
                              $('#quantity').val('');
                              $('#category').val('');
                              $('#update').val('');
                              $('#sku').attr('readonly', false);

                            }


                                          

                          },
                          error: function(xhr,er) {
                             alert(xhr);
                             alert(er);
                          }
                      }); 
                  })



      });
   </script>

</html>
