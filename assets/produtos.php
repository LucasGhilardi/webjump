<?php

include('class/Geral.php');
$class = new Geral();
$Login = new Login();
//VERIFICA SE O IP ESTA BLOQUEADO
$Login->bloqueioIp($_SERVER["REMOTE_ADDR"]);
$Login->verificaLogin();





//verifica se o usuario clicou em deletar e roda function
if ($_GET['acao']=='deletar')
{
  $class->apagarProdutos($_GET['cod']);
}



//Puxa dados da tabela category
$query = $class->listarTabela('products');



?>



<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <?php include('include/header.php'); ?>
  <!-- Header -->
  <?php include('include/superior.php'); ?>
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="addProduto.php" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <? while ($dados = $query->fetch_object()) { ?>

      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$dados->name?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$dados->sku?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">R$ <?=$dados->price?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$dados->quantity?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">
            <?php 

           $categoria =$class->listarCategorias($dados->id)?>
               <? while ($dadosc = $categoria->fetch_object()) { ?>


                <?=$dadosc->name;?>
                <br>


               <? } ?>


           </span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
           <div class="action edit"><span><a href='addProduto?id=<?=$dados->sku?>' >Edit</a></span></div>
            <div class="action delete"><span><a href='#' onclick="deleta('<?=$dados->sku?>')"> Delete</span></div>
          </div>
        </td>
      </tr>
    <? } ?>
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php include('include/footer.php'); ?>

<script type="text/javascript">
  
  function deleta(cod)
  {

var r = confirm("Deseja Excluir ? ");
if (r == true) {
 window.location='produtos?cod='+cod+'&acao=deletar';
} 
  }
</script>
 <!-- Footer --></body>

</html>
