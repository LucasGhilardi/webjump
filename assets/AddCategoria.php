<?php
include('class/Geral.php');


$class = new Geral();
$Login = new Login();

//VERIFICA SE O IP ESTA BLOQUEADO
$Login->bloqueioIp($_SERVER["REMOTE_ADDR"]);
$Login->verificaLogin();




//Verifica se esta passando ID via GET, se tiver grava valor update no campo update hidden
if (isset($_GET['id']))
{
   $var='update';

   $query = $class->dadosCategoria($_GET['id']);
   $dados = $query->fetch_object();

}



?>



<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <?php include('include/header.php'); ?>
  <!-- Header -->
  <?php include('include/superior.php'); ?>
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    
    <form id='formulario' name="formulario">
      <input type="hidden" name="update" id='update' value="<?=$var?>">
      <input type="hidden" name="id_categoria" value="<?=$_GET['id']?>">
      
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name='category-code' class="input-text" value="<?=$dados->cod?>" <? if ($var=='update') echo 'readonly'; ?>/>
        
      </div>
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="category-name" class="input-text"  value="<?=$dados->name?>"/>
        
      </div>
      <div class="actions-form">
        <a href="categorias" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php include('include/footer.php'); ?>
 <!-- Footer -->
</body>
   <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
   
   <script type="text/javascript">
     $( document ).ready(function() {



       $('#category-code').change(function() {


                     var category=$('#category-code').val();
                      $.ajax({
                          url: 'verifica_categoria.php',
                          dataType: 'html',
                          type: 'POST',
                          data: {category :category},
                         
                          success: function(data) {

                            if (data=='1')
                            {
                              alert('CODIGO JÁ CADASTRADO');
                              $('#category-code').css({'border': '1px solid red'});          
                              $('#category-code').focus();
                            }
                            else
                            {
                              $('#category-code').css({'border': '1px solid green'});
                            }                

                          },
                          error: function(xhr,er) {
                             alert(xhr);
                             alert(er);
                          }
                      }); 
                  })




      //VERIFICA SE CAMPOS ESTA PRENCHIDOS E GRAVA VIA AJAX PRODUTO
      $('#formulario').submit(function(event) {
          event.preventDefault();

          if ($('#category-name').val()=='')
          {
            alert('Digite o nome da categoria');
            $('#category-name').focus();
            return false
          }

          if ($('#category-code').val()=='')
          {
            alert('Digite o codigo da categoria');
            $('#category-code').focus();
            return false
          }
         




            var serializeDados = $('#formulario').serialize();

                      $.ajax({
                          url: 'addCategoria_cadastro.php',
                          dataType: 'html',
                          type: 'POST',
                          data: serializeDados,
                         
                          success: function(data) {



                            if (data=='errocodigo')
                            {
                              alert('Codigo já cadastrado');
                              $('#category-code').focus();
                            }
                            else
                            {
                              alert("Categoria Salva");
                              //Apaga os campos
                              $('#category-code').val('');
                              $('#category-name').val('');
                              $('#update').val('');
                              $('#category-code').attr('readonly', false);
                              
                            }


                                          

                          },
                          error: function(xhr,er) {
                             alert(xhr);
                             alert(er);
                          }
                      }); 
                  })



      });
   </script>

</html>

