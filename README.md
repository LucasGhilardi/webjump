# Projeto Desafio Web Jump

Sistema BackEnd com Login e senha e bloqueio por IP

## Tecnologias Adotadas

O sistema foi desenvolvido em PHP 7, usando JQuery e Bootstrap 4 (apenas para login), e Recaptcha Google (apenas para login)

Plugin JQuery maskMoney (para formatar o preço dos produtos)



### Pré-requisitos



```
PHP 7
MYSQL
APACHE
```

### INSTALAÇÃO


```


Para executar é necessário baixar os arquivos e importar o arquivo banco.sql 
Usuário padrão Lucas senha +Novo2020
Precisa alterar o acesso ao banco no arquivo Class/Login.php e alterar as chaves do Recaptcha arquivo index.php e arquivo Login.php (linha 46)


```

Deixei o Demo disponível no meu servidor.
https://webjump.movidoaweb.com.br

usuário lucas
senha +Novo2020

## SEGURANÇA

Adicionei uma condição onde caso o usuário digite a senha ou usuário inválido o sistema bloqueia o IP por 24h (somente se validar o Recaptcha)

Adicionei SSL e regra via .htaccess para não listar o conteúdo da pasta upload (imagens)  

Gravamos na tabela log_dados o usuario, e id da alteração tabela products e category (insert,update,delete)

## OBSERVAÇÕES

Utilizei o JQuery para verificar se o SKU existe no momento do OnChange no cadastro do produtos, e tambem para gravar as informações, caso tiver varios usuários na tela de cadastro evitamos erro de SKU duplicada, além dessa segurança na aplicação o campo no MYSQL é UNIQUE. SKU E COD_CATEGORIAS

Tela de Cadastro de Categorias - Alterei a ordem para Codigo primeiro e usei a mesma logica dos Produtos. (validar se o codigo existe no onChange e antes de gravar via AJAX)

## MELHORIAS FUTURAS

Apagar os arquivos junto com a função Deletar

Redimenciar as imagens usando GD IMAGE na tela de Cadastro de Produtos

Incluir barra de processando ao cadastrar produtos




## Autor

* **Lucas Ghilardi** 15/05/2020



## Agradecimentos

* À Equipe da WebJump por essa oportunidade


